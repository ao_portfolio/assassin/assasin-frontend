import axios from "axios";
import { createApp } from "vue";
import App from "./App.vue";
import router from "./router/index.js";
import store from "./store/auth.js";

axios.defaults.withCredentials = true;
axios.defaults.baseURL = import.meta.env.VITE_API_BASE;
axios.defaults.headers.common["Accept"] = "application/json";

const app = createApp(App);
app.use(store);
app.use(router);

app.mount("#app");
