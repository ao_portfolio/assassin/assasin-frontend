import axios from "axios";
import { createStore } from "vuex";
import router from "../router";

const store = createStore({
  modules: {
    auth: {
      namespaced: true,

      // module assets
      state: {
        loggedIn: false,
        triedAutoLogin: false,
        user: {},
      },
      getters: {
        isLoggedIn(state) {
          return state.loggedIn;
        },
        getUser(state) {
          return state.user;
        },
      },
      actions: {
        async login({ dispatch }, input) {
          // try {
          await axios.get("/sanctum/csrf-cookie");
          let data = await axios
            .post("/api/login", input)
            .then(async (response) => {
              if (response.status === 200) {
                await dispatch("fetchUser");
                this.state.triedAutoLogin = true;
              }
              return response;
            });
          return data;

          // } catch (e) {
          //   console.error(e);
          //   return;
          // }
        },
        logout({ commit }) {
          commit("general/setLoading", true, { root: true });
          if (document.cookie.indexOf("XSRF-TOKEN") === -1) {
            return;
          }
          return axios
            .post("/api/logout")
            .catch()
            .finally(() => {
              // clear cookie to prevent future auto login
              document.cookie =
                "XSRF-TOKEN=;path=/;expires=Thu, 01 Jan 1970 00:00:00 UTC;Secure;";
              commit("setLoggedIn", false);
              commit("setUser", {});
              commit("general/setLoading", false, { root: true });
              router.push({ name: "home" });
            });
        },
        async tryAutoLogin({ dispatch }) {
          if (
            !this.state.triedAutoLogin &&
            document.cookie.indexOf("XSRF-TOKEN") !== -1
          ) {
            try {
              console.log("trying autoLogin");
              this.state.triedAutoLogin = true;
              return await dispatch("fetchUser");
              // .then((response) => {
              //   console.log(response);
              //   if (response.status === 200) {
              //     return dispatch("login", null);
              //   } else return;
              // });
            } catch (error) {
              console.log(error);
              console.log("autoLogin failed");
            }
          }
          return;
        },
        fetchUser({ commit }) {
          let user = axios.get("/api/user").then((response) => {
            commit("setUser", response.data);
            commit("setLoggedIn", true);
          });
          return user;
        },
      },
      mutations: {
        setLoggedIn(state, value) {
          state.loggedIn = value;
        },
        setUser(state, value) {
          state.user = value;
        },
      },
    },
    general: {
      namespaced: true,

      state: {
        loading: false,
      },

      getters: {
        isLoading(state) {
          return state.loading;
        },
      },

      mutations: {
        setLoading(state, value) {
          state.loading = value;
        },
      },
    },
  },
});
export default store;
