import { createRouter, createWebHistory } from "vue-router";
import LogoutView from "../components/LogoutView.vue";
import LoginView from "../components/LoginView.vue";
import HomeView from "../components/HomeView.vue";
import GameView from "../components/GameView.vue";
import JoinableGamesView from "../components/JoinableGamesView.vue";
import MyGamesView from "../components/MyGamesView.vue";
import MessagesView from "../components/MessagesView.vue";
import LatestPlayerMessagesView from "../components/LatestPlayerMessagesView.vue";
import ArchivedMessagesView from "../components/ArchivedMessagesView.vue";
import store from "../store/auth.js";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "",
      component: HomeView,
      name: "home",
      props: true,
      beforeEnter: (to, from, next) => {
        // do some user authorization check
        if (store.getters["auth/isLoggedIn"]) {
          next();
        } else next({ path: "/login" });
      },
    },
    {
      path: "/games",
      component: MyGamesView,
      name: "myGames",
      props: true,
      beforeEnter: (to, from, next) => {
        // do some user authorization check
        if (store.getters["auth/isLoggedIn"]) {
          next();
        } else next({ path: "/login" });
      },
    },
    {
      path: "/games/:id",
      title: "Gameinfo",
      component: GameView,
      name: "game",
      props: true,
      beforeEnter: (to, from, next) => {
        // do some user authorization check
        if (store.getters["auth/isLoggedIn"]) {
          next();
        } else next({ path: "/login" });
      },
    },
    {
      path: "/games/:id/messages",
      component: MessagesView,
      name: "messages",
      props: true,
      beforeEnter: (to, from, next) => {
        // do some user authorization check
        if (store.getters["auth/isLoggedIn"]) {
          next();
        } else next({ path: "/login" });
      },
    },
    {
      path: "/games/:id/messages/latest",
      component: LatestPlayerMessagesView,
      name: "latestPlayerMessages",
      props: true,
      beforeEnter: (to, from, next) => {
        // do some user authorization check
        if (
          store.getters["auth/isLoggedIn"] &&
          store.getters["auth/getUser"].role !== "player"
        ) {
          next();
        } else next({ path: "/login" });
      },
    },
    {
      path: "/games/:id/messages/archived",
      component: ArchivedMessagesView,
      name: "archivedMessages",
      props: true,
      beforeEnter: (to, from, next) => {
        // do some user authorization check
        if (
          store.getters["auth/isLoggedIn"] &&
          store.getters["auth/getUser"].role !== "player"
        ) {
          next();
        } else next({ path: "/login" });
      },
    },
    {
      path: "/games/:id/players/:playerId/messages",
      component: MessagesView,
      name: "playerMessages",
      props: true,
      beforeEnter: (to, from, next) => {
        // do some user authorization check
        if (
          store.getters["auth/isLoggedIn"] &&
          store.getters["auth/getUser"].role !== "player"
        ) {
          next();
        } else next({ path: "/login" });
      },
    },
    {
      path: "/games/joinable",
      component: JoinableGamesView,
      name: "joinGame",
      props: true,
      beforeEnter: (to, from, next) => {
        // do some user authorization check
        if (
          store.getters["auth/isLoggedIn"] &&
          store.getters["auth/getUser"].role === "player"
        ) {
          next();
        } else next({ path: "/login" });
      },
    },
    {
      path: "/login",
      component: LoginView,
      name: "login",
      props: true,
      beforeEnter: (to, from, next) => {
        // do some user authorization check
        if (!store.getters["auth/isLoggedIn"]) {
          next();
        } else next({ path: "/" });
      },
    },
    {
      path: "/logout",
      component: LogoutView,
      name: "logout",
      props: true,
      beforeEnter: (to, from, next) => {
        // do some user authorization check
        if (store.getters["auth/isLoggedIn"]) {
          next();
        } else next({ path: "/login" });
      },
    },
    { path: "/:catchAll(.*)", redirect: { path: "/" } },
  ],
});

router.beforeEach(async (to, from, next) => {
  store.commit("general/setLoading", true);
  await store.dispatch("auth/tryAutoLogin");
  store.commit("general/setLoading", false);
  next();
});
export default router;
