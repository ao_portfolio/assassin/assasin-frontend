// MyComponent.test.js
import { test, describe, expect } from "vitest";
import router from "../src/router/index";

describe("router", () => {
  test("every route has properties enabled", () => {
    expect(
      router.getRoutes().every((r) => {
        // console.log(r);
        if (r.path === "/:catchAll(.*)") return true;
        return r.props?.default;
      })
    ).toBe(true);
  });
});
