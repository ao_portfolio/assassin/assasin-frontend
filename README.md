# Project description

"Assassin" is een populair real-life, sociaal of campusbreed spel dat vaak wordt gespeeld door groepen mensen. Dit is de frontend voor zo'n spel gemaakt voor een schoolopdracht met het frontend-framework Vue.


## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
